<?php

namespace App\Http\Controllers\AdminControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use DataTables;
use App\Models\User;
use App\Models\Like;
use App\Models\Follow;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {
       
        $users=User::where('status',1)->get(); 
        // return $users;
        if($request->ajax()){
            return DataTables::of($users)
            ->addColumn('image',function($user){
                return '<img class="rounded-circle" height="50px" width="50px" src='.$user->photo.'>';
                
            })
            ->addColumn('status',function($user){
                if($user->status==0)
                    return 'de-active';
                return 'Active';
            })
            ->addColumn('action',function($user){
                $button='';
                if($user->status==0){
                    $cls='fa-check';
                }elseif($user->status==1){
                    $cls= 'fa-times';
                }
               
                 $button .= '<a class="mr-2" href=""><i class="fa fa-tv"></i></a>';
                $button .= '<a class="mr-2" href=""><i class="fa '.$cls.'"></i></a>';
                $button .= '<a title="Followings" class="mr-2" href="'.route("user.followings",["id"=>$user->id]).'"><i class="fa fa-thumbs-up"></i></a>';
                $button .= '<a title="Likes" class="mr-2" href="'.route("user.likings",["id"=>$user->id]).'"><i class="fa fa-heart"></i></a>';

                $button .= '<a class="mr-2" href=""><i class="fa fa-trash"></i></a>';
               
               
                return $button;
            })
            ->rawColumns(['image','status','action'])
            ->make(true);
            
        }
        return view('admin.users.index');
        
    }

    public function user_likings(Request $request,$id=null,$type=null){
        $users=Like::where('user_id',$id)->where('status',1)->with('likings:id,name,email,phone,photo,country')->get(['id','user_id','tobeliked']);
        if($request->ajax()){
            return DataTables::of($users)
            ->make(true);
        }
        


        return view('admin.users.like',compact('id','type'));
    }
    

     public function user_followings(Request $request,$id=null,$type=null)
    {
       $users=Follow::where('user_id',$id)->where('status',1)->with('follows:id,name,email,phone,country')->get(['id','user_id','tobefollowed']);
        if($request->ajax()){
            return DataTables::of($users)
            ->addColumn('image',function($users){
                return '<img class="rounded-circle" height="50px" width="50px" src='.asset('storage/profile_images/'.$users->photo).'>';
                
            })
            ->addColumn('status',function($users){
                if($users->status==0)
                    return 'de-active';
                return 'Active';
            })
            ->addColumn('name',function($users){
               
                return $users->follows->name;
            })
            ->addColumn('action',function($users){
                $button='';
                if($users->status==0){
                    $cls='fa-check';
                }elseif($users->status==1){
                    $cls= 'fa-times';
                }
               
                 $button .= '<a class="mr-2" href=""><i class="fa fa-tv"></i></a>';
                $button .= '<a class="mr-2" href=""><i class="fa '.$cls.'"></i></a>';
                $button .= '<a title="Follows" class="mr-2" href=""><i class="fa fa-thumbs-up"></i></a>';
                $button .= '<a title="Likes" class="mr-2" href=""><i class="fa fa-heart"></i></a>';

                $button .= '<a class="mr-2" href="'.route("user.del",["id"=>$users->id]).'"><i class="fa fa-trash"></i></a>';
               
               
                return $button;
            })
            ->rawColumns(['image','status','action'])
            ->make(true);
            
        }
        return view('admin.users.follow',compact('id','type'));
        
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $user=User::find($id);
        try{
            DB::beginTransaction();
            $user->delete();
            DB::commit();
            return back()->with('success','user deleted successfully');
        }catch(\Exception $e){
            DB::rollback();
            if(env("APP_ENV")=="local"){
                dd($e);
            }
            return back()->with('error','user could not be deleted');
        }
    }
}
