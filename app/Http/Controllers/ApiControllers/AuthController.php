<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Models\User;
use App\Models\Follow;
use App\Models\Photo;
use App\Models\Video;
use Illuminate\Database\Eloquent\Builder;
use App\Mail\forgot_pwd;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use GeoIP;
use Image;


class AuthController extends Controller
{
	// $user_attributes_array=['name','email','phone','country','gender','relationship_status','dob','education','body_type','have_children','zodiac_sign','sexual_orientation','smoker','drink','sports','height','hair_color','eye_color','ethnicity','status','is_social','photo','verify_token','city','postal_code',
 //        'currency','lon','state_name','i_am','lk_for','min_age','max_age','s_photos','s_online','s_r_status','s_country',];
    //

    protected $attrToUser=['id','name','email','phone','country','city','gender','relationship_status','dob','education','body_type','have_children','zodiac_sign','sexual_orientation','smoker','drink','sports','height','hair_color','eye_color','ethnicity','is_social','photo'];


    public function token_key(){
    	return 'testToken';
    }
    
   

    public function register(Request $request){

    	$validator = Validator::make($request->all(), [
	      'email' => 'required|unique:users',
	      'password' => 'required',
	    ]);

	    if ($validator->fails()) {
	      return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
	    }
	    try{
	    	DB::beginTransaction();
	    	$user=User::create([
	    		"email"=>$request->email,
	    		"password"=>Hash::make($request->password)
			]);
			$token=$user->createToken($this->token_key())->accessToken;
	    	DB::commit();
	    	return response()->json(['status'=> 1, 'token'=>$token]);
	    }
	    catch(\Exception $e){
	    	DB::rollback();
	    	if(env("APP_ENV"=="local")){
	    		dd($e);
	    	}else{
	    		return response()->json(['status'=>0,'message'=>'database error']);
	    	}
	    
	    }
	   
    }
    public function social_register(Request $request){
	  $validator = Validator::make($request->all(), [
	      'email' => 'required|unique:users',
	      'is_social' => 'required',
	  ]);
	  if ($validator->fails()) {
	      return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
	    }
	     try{
	    	DB::beginTransaction();
	    	$user=User::create([
	    		"email"=>$request->email,
	    		"is_social"=>$request->is_social
	    		
			]);
			$token=$user->createToken($this->token_key())->accessToken;
	    	DB::commit();
	    	return response()->json(['status'=> 1, 'token'=>$token]);
	    }
	    catch(\Exception $e){
	    	DB::rollback();
	    	if(env("APP_ENV"=="local")){
	    		dd($e);
	    	}else{
	    		return response()->json(['status'=>0,'message'=>'database error']);
	    	}
	    
	    }
	}
    public function save_user_info(Request $request){
    	// gencode
    	$user = auth::guard('api')->user();
    	if($user->name==null || $user->phone==null || $user->country==null ||$user->gender==null || $user->dob==null){
	    	$validator = Validator::make($request->all(), [
		      "name"=>"required",
		      "phone"=>"required|min:10|max:13",
		      "country"=>"required",
		      "gender"=>"required",
		      "dob"=>"required",
		    ]);
		  	if ($validator->fails()){
		      return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
		    }
    	}
    	
    	
    	
    	$ip= $request->ip();
    	// return geoip()->getLocation($ip)->state_name;
		
    	try{
    		DB::beginTransaction();
			$data = $request->all();
			$data['country']=$request->country;
			$data['postal_code']=geoip()->getLocation($ip)->postal_code;
			// $data['city']=geoip()->getLocation($ip)->city;
			// $data['state_name']=geoip()->getLocation($ip)->state_name;
			$data['lat']=geoip()->getLocation($ip)->lat;
			$data['lon']=geoip()->getLocation($ip)->lon;
			// $data['currency']=geoip()->getLocation($ip)->currency;
			if($request->password){
    			$data['password']=hash::make($request->password);
    		}
    		if($request->hasFile('photo')){
    			$photo=$request->file('photo');
    			$photoName=uniqid().'.'.$photo->getClientOriginalExtension();
    			$photo = Image::make($photo);
    			$photo->resize(500, 500, function ($constraint) {
				    $constraint->aspectRatio();
				});
				$resource = $photo->stream()->detach();
				Storage::disk('s3')->put('images/' . $photoName,$resource,'public');
				$data['photo']=$photoName;
				
				
			}
    		$user->update($data);
    		DB::commit();
    		return response()->json(['status'=>1,'message'=>'user information saved successfully']);
    	}
    	catch(\Exception $e){
    		if(env("APP_ENV")=="local"){
    			dd($e);
    		}else{
    			return response()->json(['status'=>0,'message'=>'database error']);
    		}
    	}
	}

	// public function update_user_info(Request $request){
	// 	$user = auth::guard('api')->user();
 //    	if($user->name==null || $user->phone==null || $user->country==null ||$user->gender==null || $user->dob==null){
	//     	$validator = Validator::make($request->all(), [
	// 	      "name":"required",
	// 	      "phone":"required|min:10|max:13",
	// 	      "country":"required",
	// 	      "gender":"required",
	// 	      "dob":"required",
	// 	    ]);
	// 	  	if ($validator->fails()){
	// 	      return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
	// 	    }
 //    	}
    	
    	
    	
 //    	$ip= $request->ip();
 //    	// return geoip()->getLocation($ip)->state_name;
		
 //    	try{
 //    		DB::beginTransaction();
	// 		$data = $request->all();
	// 		$data['country']=$request->country;
	// 		$data['postal_code']=geoip()->getLocation($ip)->postal_code;
	// 		// $data['city']=geoip()->getLocation($ip)->city;
	// 		// $data['state_name']=geoip()->getLocation($ip)->state_name;
	// 		$data['lat']=geoip()->getLocation($ip)->lat;
	// 		$data['lon']=geoip()->getLocation($ip)->lon;
	// 		// $data['currency']=geoip()->getLocation($ip)->currency;
	// 		if($request->password){
 //    			$data['password']=hash::make($request->password);
 //    		}
 //    		if($request->hasFile('photo')){
 //    			$photo=$request->file('photo');
 //    			$photoName=uniqid().'.'.$photo->getClientOriginalExtension();
 //    			$photo = Image::make($photo);
 //    			$photo->resize(300, 300, function ($constraint) {
	// 			    $constraint->aspectRatio();
	// 			});
	// 			$resource = $photo->stream()->detach();
	// 			Storage::disk('s3')->put('images/' . $photoName,$resource,'public');
	// 			$data['photo']=$photoName;
				
				
	// 		}
 //    		$user->update($data);
 //    		DB::commit();
 //    		return response()->json(['status'=>1,'message'=>'user information saved successfully']);
 //    	}
 //    	catch(\Exception $e){
 //    		if(env("APP_ENV")=="local"){
 //    			dd($e);
 //    		}else{
 //    			return response()->json(['status'=>0,'message'=>'database error']);
 //    		}
 //    	}
	// }

	// public function reset_password(){
	// 	if($request->password){
 //    		$validator = Validator::make($request->all(),[
 //    			'email' => 'required',
	// 	    	'password' => 'same:password_confirmation',
	// 		]);
	// 		if ($validator->fails()) {
	// 			return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
	// 		}
	// 		$data['password']=hash::make($request->password);
	// 	}
		
	// }


	// public function save_user_image(Request $request){

	// 	if($request->hasFile('photo')){
	// 		$user=auth()->guard('api')->user();
	// 		$photo=$request->file('photo');
	// 		$photoName=uniqid().'.'.$photo->getClientOriginalExtension();
	// 		try{

	// 			DB::beginTransaction();
	// 			$user->update(["photo"=>$photoName]);
	// 			Storage::disk('public')->put('profile_images/'.$photoName, file_get_contents($request->file('photo')),'public');
	// 			DB::commit();
	// 			return response()->json(['status'=>1,'message'=>1208]);
	// 		}
	// 		catch(\Exception $e){
	// 			DB::rollback();
	// 			if(env("APP_ENV"=="local")){
	// 				dd($e);
	// 			}else{
	// 			return response()->json(['status'=>0,'message'=>1209]);
	// 			}
	// 		}
	// 	}
	// }
	public function get_online_users(Request $request){
		$result=[];
		$users=User::where('status',1)->get($this->attrToUser);
		foreach ($users as $user) {
			if($user->isOnline){
				array_push($result, $user);
			}
		}
		return $result;
	}
	public function get_user_info(Request $request){
		$user=auth::guard('api')->user();
		$followers=Follow::where('following_id',$user->id)->count();
		$images=Photo::where('user_id',$user->id)->count();
		$videos=Video::where('user_id',$user->id)->count();
		$user=$user->only($this->attrToUser);
		return response()->json([
			'status'=>1,
			'profile_completion'=>$this->profile_completion_percentage($user),
			'followers'=>$followers,
			'videos'=>$videos,
			'photos'=>$images,
			'user_details'=>$user
		]);
	}

	public function get_code_gmail(Request $request){
		$validator = Validator::make($request->all(), [
	      'email' => 'required'
	    ]);
	    if($validator->fails()) {
	      return response()->json(['status' => 0, 'message'=>$this->get_errors($validator->messages())]);
	    }
	    $code= rand(100000,999999);
	    $details=[
	    	'title' => 'Verification Code',
	    	'body' => 'Your six digit verification code is '.$code
	    ];
	    $details='Your six digit verification code is '.$code;
	    try{
	    	DB::beginTransaction();
	    	$user=User::where('email',$request->email)->first();
	    	$user->update(["verify_token"=>$code]);
	    	Mail::to($request->email)->send(new forgot_pwd($code));
			if(Mail::failures()) {
	            return response()->json(['status'=> 0,'message'=>'Email cannot be sent! try later']);
	        }
	        DB::commit();
	        return response()->json(['status'=> 1,'message'=>'A mail has been sent to given mail.Please verify code']);
	    }
	    catch(\Exception $e){
	    	DB::rollback();
	    	if(env("APP_ENV")=="local"){
	    		return $e;
	    	}else{
	    		return response()->json(['status'=> 0,'message'=>'database error in email sending']); 
	    	}
	    }
	}
	public function confirm_code(Request $request){
		$user=User::where('email',$request->email)->where('verify_token',$request->token)->first();
		if($user){
			try{
				DB::beginTransaction();
				$user->update(['verify_token'=>1]);
				$user->tokens->each(function($token, $key) {
			        $token->delete();
			    });
				DB::commit();
				$token=$user->createToken($this->token_key())->accessToken;
				return response()->json(['status'=>1,'message'=>'token is verified! reset password','token'=>$token]);
			}catch(\Exception $e){
				DB::rollback();
				if(env("APP_ENV")=="local"){
					dd($e);
				}else{
					return response()->json(['status'=>0,'message'=>'database error']);
				}
			}
		}else{
			return response()->json(['status'=>0,'message'=>'wrong verification code']);
		}
	}

	protected $allSearchedUser;
	public function search_match(Request $request){
		$allusers=User::where('status',)->paginate(2);
		return $allusers;
		$allattributes=$request->except(['page']);
		$i=0;
		foreach ($allattributes as $key => $value){

			if($key=='minAge' || $key=='maxAge'){
				continue;
			}
			$user=User::where($key,$value)->get($this->attrToUser);
			if($i==0){
				$this->allSearchedUser=$user;
				++$i;
			}else{
				$this->allSearchedUser=$this->allSearchedUser->merge($user);
			}
		}
		if($request->has(['minAge','maxAge'])){
			$minAge=Carbon::today()->subYears(10);
			$maxAge=Carbon::today()->subYears(12);
			$user=User::whereBetween('dob',[$maxAge,$minAge])->get();
			if($this->allSearchedUser){
				$this->allSearchedUser=$this->allSearchedUser->merge($user);
			}
			else{
				$this->allSearchedUser=$user;	
			}
			
		}

		return response()->json(['status'=>1,'users'=>$this->allSearchedUser]);


		// if($request->has('page')){
		// 	$page=$request->page;
		// 	if($page<=0 || $page==null)
		// 		$page=1;
		// }else{
		// 	$page=1;
		// }

		// $total_records=count($this->allSearchedUser);
		
		// if($total_records%10>0){
		// 	$total_pages= (int)($total_records/10)+1;
		// }else{
		// 	$total_pages= ($total_records/10);
		// }
		
		// $finalUsers=[];
		// $temp=($page-1)*10;
		// $till=$temp+10;
		// if($page<=$total_pages){
		// 	$count=0;
		// 	for($i=0;$i<$total_records;$i++) {
		// 		if($i>=$temp && $i<$till){
					
		// 		}else{
		// 			unset($this->allSearchedUser[$i]);
		// 		}
		// 	}
		// }
		// // return array_values(array($this->allSearchedUser));
		// return response()->json(['status'=>1,'total_users'=>$this->allSearchedUser->count(),'page'=>$page,'total_page'=>$total_pages,'users'=>$this->allSearchedUser]);
	}


	public function basic_search(Request $request){
		// return $request->all();
		$user=auth()->guard('api')->user();
		if($request->has('i_am') && $request->has('lk_for') && $request->has('min_age') && $request->has('max_age') && $request->has('s_r_status') && $request->has('s_country')){
			$user->update($request->all());
			return response()->json(['status'=>1,'message'=>'search criteria has been saved']);
		}
		$minAge = $user->min_age;
		$maxAge = $user->max_age;
		$minDate = Carbon::today()->subYears($maxAge); // make sure to use Carbon\Carbon in the class
		$maxDate = Carbon::today()->subYears($minAge)->endOfDay();
		// return User::whereBetween('dob',[$minDate,$maxDate])->get();
		// return $user->i_am;$this->attrToUser
		$users=User::whereNotIn('id',$user->id)->where('status',1)->where('gender',$user->lk_for)->orWhere('country',$user->s_country)->orWhere('relationship_status',$user->s_r_status)->orWhereBetween('dob',[$minDate,$maxDate])->paginate(5,$this->attrToUser);
		$users=$users->toArray();
		unset($users["links"]);
		unset($users["from"]);
		unset($users["last_page"]);
		unset($users["last_page_url"]);
		unset($users["per_page"]);
		unset($users["to"]);
		$users['status']=true;
		return $users;
	}

	public function user_profile(Request $request){
		$user=auth()->guard('api')->user();
		return response()->json(['status'=>true,'profile_completion'=>$this->profile_completion_percentage($user),'user_profile'=>$user]);
		// return $user;
	}

	public function search_nearest(Request $request){
		$ip=$request->ip();
		$allattributes= geoip()->getLocation($ip)->toArray();
		$data['country']=$allattributes['country'];
		$data['city']=$allattributes['city'];
		$data['state_name']=$allattributes['state_name'];
		$data['postal_code']=$allattributes['postal_code'];
		$data['lat']=$allattributes['lat'];
		$data['lon']=$allattributes['lon'];
		$i=0;
		foreach ($data as $key => $value){
			
			$user=User::where($key,$value)->get($this->attrToUser);
			if($i==0){
				$this->allSearchedUser=$user;
				++$i;
			}else{
				$this->allSearchedUser=$this->allSearchedUser->merge($user);
			}
		}
		return response()->json(['status'=>1,'total_users'=>$this->allSearchedUser->count(),'users'=>$this->allSearchedUser]);
	}

	public function get_errors($errors){
		
    	foreach ($errors->get('*') as $key => $value){
    		return $value[0];
    		// $associativeArray += [$key => $value[0]];
    		// return $value[0];
    	}


		
	}

	public function profile_completion_percentage($user){
		if(gettype($user)!='array'){
			$user= $user->toArray();	
		}
		unset($user["isOnline"]);
		unset($user["created_at"]);
		unset($user["updated_at"]);
		unset($user["email_verified_at"]);
		unset($user["lat"]);
		unset($user["lon"]);
		unset($user["postal_code"]);
		unset($user["state_name"]);
		unset($user["verify_token"]);
		unset($user["is_social"]);
		unset($user["status"]);
		unset($user["id"]);
		// unset($user["name"]);
		$percentage=0;
		$per_field=100/count($user);
		foreach ($user as $key => $value){
			if($value!=null){
				$percentage +=$per_field;
			}
		}
		if((int)$percentage==99){
			return 100;
		}
		return (int)$percentage;
	}

	
	

    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
	      'email' => 'required',  //email may be email or phone number
	      'password' => 'required',
	    ]);
		if ($validator->fails()) {
			return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
	    }
	    if(is_numeric($request->email)){
			$credentials = [
		        'phone' => $request->email,
		        'password' => $request->password
		    ];
		}elseif(filter_var($request->email)){
		    $credentials = [
		        'email' => $request->email,
		        'password' => $request->password
		     ];
	    }

	    if(auth()->attempt($credentials)){
	    	$user=Auth::user();
	    	$token=$user->createToken($this->token_key())->accessToken;
	    	// $user= collect($user)->except('id','status','is_social','email_verified_at','verify_token','created_at','updated_at');
	    	$user=$user->only($this->attrToUser);
	    	return response()->json(['status' => 1,'user_details'=>$user, 'token' => $token]);
		}else{
			return response()->json(['status' => 0, 'message'=>'Incorrect email or password']);
		}
	}

	public function logout(Request $request){
		$user=auth::guard('api')->user();
    	if($user->tokens){
    		foreach ($user->tokens as $token) {
    			$token->delete();
    		}
    	}
    	return response()->json(['status' => 1,'status_code'=>"logged out"]);
    }

    public function get_all_users(Request $request){
    	if($request->has('page')){
    		$page=$request->page;
    		if($page<=0 || $page==null){
    			$page=1;
    		}
    		
    	}elseif(!$request->has('page')){
    		$page=1;
    	}
		$temp=($page-1)*10;
		$user=User::skip($temp)->take(10)->get($this->attrToUser);
    	
    	if(!count($user)){
    		$user="No more users";
    	}
    	return response()->json(['status'=>1,'page'=>$page,'users'=>$user]);
    	
    }
   
}
