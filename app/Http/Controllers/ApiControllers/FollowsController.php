<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Validator;
use DB;
use App\Models\User;
use App\Models\Follow;

class FollowsController extends Controller
{
   
    public function index()
    {
        //
    }
    public function to_follow(Request $request){
        $validator = Validator::make($request->all(), [
          'tobefollowed' => 'required',
        ]);
        if($validator->fails()){
          return response()->json(['status' => 0, 'errors' => $validator->errors()]);
        }
        if($follow=Follow::where('user_id',auth()->guard('api')->user()->id)->where('tobefollowed',$request->tobefollowed)->first()){
            try{
                DB::beginTransaction();
                if($follow->status==0){
                    $follow->update(['status'=>1]);
                    $msg="Followed";
                }else{
                    $follow->update(['status'=>0]);
                    $msg="Un-Followed";
                }
                DB::commit();
                return response()->json(['status'=> 1, 'status_msg'=>$msg]);
            }catch(\Exception $e){
                DB::rollback();
                return response()->json(['status'=> 0, 'status_msg'=>'Database_error',$e]);
            }
        }else{
            try{
                DB::beginTransaction();
                Follow::create([
                    'user_id'=>auth()->guard('api')->user()->id,
                    'tobefollowed'=>$request->tobefollowed
                ]);
                DB::commit();
                return response()->json(['status'=> 1, 'status_msg'=>'Followed']);
            }
            catch(\Exception $e){
                DB::rollback();
                return response()->json(['status'=> 0, 'status_msg'=>'Un-Followed',$e]);
            }
        }

        //gen code

        // try{
        //     DB::beginTransaction();
        //     Follow::create([
        //         'user_id'=>auth()->guard('api')->user()->id,
        //         'tobefollowed'=>$request->tobefollowed
        //     ]);
        //     DB::commit();
        //     return response()->json(['status'=> 1, 'status_msg'=>'Followed']);

        // }
        // catch(\Exception $e){
        //     DB::rollback();
        //     return $e;
        // }
        
    }
    
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
