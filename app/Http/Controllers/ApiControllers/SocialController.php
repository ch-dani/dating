<?php

namespace App\Http\Controllers\ApiControllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Video;
use App\Models\Like;
use App\Models\Follow;
use App\Models\Photo;
use Validator;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use DB;
use Illuminate\Database\Eloquent\Builder;
use App\Mail\forgot_pwd;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use GeoIP;
use Image;

class SocialController extends Controller
{

    public function upload_image(Request $request){
        $validator = Validator::make($request->all(),[
          'photo' => 'required',
          'status'=>'required',
        ]);
        if($validator->fails()){
            return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
        }
        $user=auth()->guard('api')->user();

        try{
            DB::beginTransaction();
            $photo=$request->file('photo');
            $photoName=uniqid().'.'.$photo->getClientOriginalExtension();
            $photo = Image::make($photo);
            $photo->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();
            });
            $resource = $photo->stream()->detach();
            Storage::disk('s3')->put('galleryImages/' . $photoName,$resource,'public');
            Photo::create([
                "user_id"=>$user->id,
                "image"=>$photoName,
                "status"=>$request->status,
            ]);
            DB::commit();
            return response()->json(['status' =>1,'message'=>'Image uploaded Successfully!']);
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(['status' =>0,'message'=>'Image cannot be uploaded!',"errors"=>$e]);
        }
    }
    public function upload_video(Request $request){
        return 'pending uploading video';
    }
    public function like_image(Request $request){
        $validator = Validator::make($request->all(),[
          'photoid' => 'required|min:1',
        ]);
        if($validator->fails()){
            return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
        }
        $image=Photo::find($request->photoid);
        $user=auth()->guard('api')->user();
        $alreadyLiked=Like::where('user_id',$user->id)->where('image_id',$request->photoid)->count();

        if($alreadyLiked){
            return response()->json(['status' => 0,'message'=>"You already liked ".$image->user->name."'s this image"]);
        }
        
        try{
            DB::beginTransaction();
            Like::create([
                "user_id"=>$user->id,
                "image_id"=>$request->photoid,
                "video_id"=>null,
                "status"=>1
            ]);
            DB::commit();
            return response()->json(['status' => 1,'message'=>"You liked ".$image->user->name."'s image"]);
        }
        catch(\Exception $e){
            DB::rollback();
            return response()->json(['status' =>0,'error'=>$e]);
        }
        

        return 'liked galleryImages image';
    }
    public function like_video(Request $request){
        return 'pending like video';
    }
    public function follow_user(Request $request){
        $validator = Validator::make($request->all(), [
          'id' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json(['status' => 0,'message'=>$this->get_errors($validator->messages())]);
        }
        $ifFollowingExists=User::find($request->id);
        $follower=auth()->guard('api')->user();
        $ifAlreadyFollowed=Follow::where('user_id',$follower->id)->where('following_id',$request->id)->count();
        if($ifAlreadyFollowed){
            return response()->json(['status' => 0,'message'=>'You already followed '.$ifFollowingExists->name]);
        }
        if($ifFollowingExists){
            if($request->id==$follower->id){
                return response()->json(['status' => 0,'message'=>'User can not follow himself']);
            }else{
                try{
                    DB::beginTransaction();
                    Follow::create([
                        "user_id"=>$follower->id,
                        "following_id"=>$request->id,
                        "status"=>1
                    ]);
                    DB::commit();
                     return response()->json(['status' =>1,'message'=>'You followed '.$ifFollowingExists->name]);
                }
                catch(\Exception $e){
                    DB::rollback();
                    return response()->json([$e]);
                }
            }
        }else{
            return response()->json(['status' => 0,'message'=>'User does not exist']);
        }
    }
    public function get_user_all_gallery(Request $request){
        $follower=auth()->guard('api')->user();
        $allImages=Photo::where('user_id',$follower->id)->orderBy('created_at','desc')->get(['id','image','status']);
        return response()->json(['status' =>1,'gallery'=>$allImages]);
    }
    public function get_user_public_gallery(Request $request){
        $follower=auth()->guard('api')->user();
        $allImages=Photo::where('user_id',$follower->id)->where('status',1)->get(['id','image','status']);
        return response()->json(['status' =>1,'gallery'=>$allImages]);
    }
    public function get_user_private_gallery(Request $request){
        $follower=auth()->guard('api')->user();
        $allImages=Photo::where('user_id',$follower->id)->where('status',0)->get(['id','image','status']);
        return response()->json(['status' =>1,'gallery'=>$allImages]);
    }


   
  

   
    public function get_errors($errors){
        foreach ($errors->get('*') as $key => $value){
            return $value[0];
        }
    }
}
