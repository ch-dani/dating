<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class forgot_pwd extends Mailable
{
    use Queueable, SerializesModels;
    public $details;
    public $subject='Forgot passwords';
    public $code;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($code)
    {
        return $this->code=$code;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('findyourmatchere@gmail.com')->subject($this->subject)->view("mails.forgot_password",["data"=>$this->code]);
        // return $this->view('mails.forgot_password');
    }
}
