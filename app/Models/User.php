<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Storage;
use Cache;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'country',
        'gender',
        'relationship_status',
        'dob',
        'education',
        'body_type',
        'have_children',
        'zodiac_sign',
        'sexual_orientation',
        'smoker',
        'drink',
        'sports',
        'height',
        'hair_color',
        'eye_color',
        'ethnicity',
        'status',
        'is_social',
        'photo',
        'password',
        'verify_token',
        'city',
        'postal_code',
        'currency',
        'lat',
        'lon',
        'state_name',
        'i_am',
        'lk_for',
        'min_age',
        'max_age',
        's_photos',
        's_online',
        's_r_status',
        's_country',
    ];
    protected $hidden = [
        'password',
        'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    protected $appends = array('isOnline');

    public function getPhotoAttribute(){
        return ($this->attributes['photo'] != null) ? Storage::disk('s3')->url('images/'.$this->attributes['photo']) : null;
    }

    public function getIsOnlineAttribute(){
        return (Cache::get('user-is-online-'.$this->id)!=null)?true:false;
    }

    // public function isOnline(){
    //     return Cache::get('user-is-online-' . $this->id);
    // }

  


   
}
