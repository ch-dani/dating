<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name'=>'admin',
        	'email'=>'admin@gmail.com',
        	'phone'=>'+923457872616',
        	'country'=>92,
        	'status'=>1,
        	'is_social'=>0,
        	'password'=>hash::make('password'),
        	'photo'=> '',

        ]);
    }
}
