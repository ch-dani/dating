@extends('admin.layouts.master')
@section('title','followings')
@section('main_content')
	<div>

		<table class="table table-hover data-table user_table">
			<thead>
				<tr>
					<th>#</th>
					<th>Image</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					
					<!-- <th>Action</th> -->
				</tr>
			</thead>
		</table>
	</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
	

	<script type="text/javascript">
		$("document").ready(function(){

		})
		
		$(".user_table").DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('user.followings',['id'=>$id,'type'=>$type]) }}",
        'dataSrc': 'follows',
        columns: [
            {
            	data: 'id', 
            	name: 'id'
            },
            {
            	data: 'image',
            	name: 'image'
            },

            {
            	data: 'name', 
            	name: 'name'
            },
            {
            	data: 'follows.email', 
            	name: 'follows.email'
            },
            {
            	data: 'follows.phone', 
            	name: 'follows.phone'
            }
           
            
            
           
        ]
    });
		
	</script>
@endsection