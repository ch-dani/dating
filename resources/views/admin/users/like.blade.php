@extends('admin.layouts.master')
@section('title','likes')
@section('main_content')
	<div>

		<table class="table table-hover data-table user_table">
			<thead>
				<tr>
					<th>#</th>
					<th>Image</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					
					<!-- <th>Action</th> -->
				</tr>
			</thead>
		</table>
	</div>
@endsection
@section('scripts')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
	

	<script type="text/javascript">
		$("document").ready(function(){

		})
		
		$(".user_table").DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('user.likings',['id'=>$id,'type'=>$type]) }}",
        'dataSrc': 'follows',
        columns: [
            {
            	data: 'id', 
            	name: 'id'
            },
            {
            	data: 'likings.photo',
            	name: 'likings.photo'
            },

            {
            	data: 'likings.name', 
            	name: 'likings.name'
            },
            {
            	data: 'likings.email', 
            	name: 'likings.email'
            },
            {
            	data: 'likings.phone', 
            	name: 'likings.phone'
            }
           
            
            
           
        ]
    });
		
	</script>
@endsection